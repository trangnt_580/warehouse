package controllers;
import play.mvc.Controller;
import play.mvc.Result;
import play.data.Form;
import views.html.products.list;
import views.html.products.details;
import java.util.*;
import models.*;
//import models.Tag;
//import models.StockItem;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;
import play.mvc.Security;

@Security.Authenticated(Secured.class)
public class Products extends Controller {
	private static final Form<Product> productForm = Form.form(Product.class);
	
	public static Result list (Integer page){
		Page<Product> products = Product.find(page);
		return ok(views.html.catalog.render(products));
	}
	public static Result newProduct (){
		return ok(details.render(productForm));
	}
	public static Result details(Product product){
		if (product == null) {
			return notFound(String.format("Product %s does not exist.", product.ean));
		}
		Form<Product> filledForm = productForm.fill(product);
		return ok(details.render(filledForm));
	}
	public static Result delete(Product product) {
		if(product == null) {
		return notFound(String.format("Product %s does not exists.", product.ean));
		}
		for(StockItem stockItem : product.stockItems){
			stockItem.delete();
		}
		product.delete();
		return redirect(routes.Products.list(0));
	}
	public static Result save(){
		Form<Product> boundForm = productForm.bindFromRequest();
		if(boundForm.hasErrors()){
			flash("error", "Please correct the form below.");
			return badRequest(details.render(boundForm));
		}
		Product product = boundForm.get();
		List<Tag> tags = new ArrayList<Tag>();
		for(Tag tag: product.tags){
			if(tag.id != null){
				tags.add(Tag.findById(tag.id));
			}
		}
		product.tags = tags;
		if( product.id == null){
			Ebean.save(product);
		}else{
			Ebean.update(product);
		}
		StockItem stockItem = new StockItem();
		stockItem.product = product;
		stockItem.quantity = 0L;
		stockItem.save();
		flash("success",String.format("Sucessfully added product %s", product));
		return redirect(routes.Products.list(0));
	}
}