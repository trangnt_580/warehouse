package models;

import java.util.*;
import javax.persistence.*;
import play.db.ebean.Model;

@Entity
public class Warehouse extends Model{
	@Id
	public Long id;
	
	public String name;
	
	@OneToMany(mappedBy="warehouse")
	public List<StockItem> stock = new ArrayList();//truong quan he
	
	@OneToOne
	public Address address;
	
	public String toString(){
		return name;
	}
}
	