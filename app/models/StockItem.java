package models;
import play.db.ebean.Model;
import javax.persistence.*;

@Entity
public class StockItem extends Model{
	@Id
	public Long id;
	@ManyToOne
	public Warehouse warehouse;     //truong quan he noi voi Warehouse
	
	@ManyToOne
	public Product product;         //truong quan he noi voi Product
	public Long quantity;
	
	public String toString(){
		return String.format("StockItem %d - %d x product %s" ,id, quantity, product==null ? null : product.id);
	}
	public static Finder<Long,StockItem> find = new Finder<Long,StockItem>(
        Long.class, StockItem.class);
}