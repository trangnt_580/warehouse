package models;

import play.data.validation.Constraints;
import java.util.*;
import javax.persistence.*;
import play.db.ebean.Model;

@Entity
public class Tag extends Model{
	@Id
	public Long id;
	
	@Constraints.Required
	public String name;
	
	@ManyToMany(mappedBy = "tags")
	public List<Product> products;
	
	public static Finder<Long, Tag> find = new Finder<>(Long.class, Tag.class);
	public Tag(){}
	public Tag(Long id, String name, Collection<Product> products){
		this.id = id;
		this.name = name;
		this.products = new LinkedList<Product>(products);
		for(Product product: products){
			product.tags.add(this);
		}
	}
	
	/*private static List<Tag> tags = new LinkedList<Tag>();
	
	static{
		tags.add(new Tag(1L,"lightweight",Product.findByName("paperclips 1")));
		tags.add(new Tag(2L,"metal",Product.findByName("paperclips")));
		tags.add(new Tag(3L,"plastic",Product.findByName("paperclips")));
	}*/
	public static Tag findById(Long id){
		return find.byId(id);
	}
}